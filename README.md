# sudo's unit - sunit

An extremly minimal unit testing framework for C.

## How to use

There are two different types of test cases: Facts and Theories.

A fact passes if it returns `true`, else it fails.

Fact example:
```c
#include <math.h>

#include "test.h"

FACT(NAN_not_equal_NAN)
{
  return NAN != NAN;
}

FIXTURE_BEGIN

RUN_FACT(NAN_not_equal_NAN);

FIXTURE_END
```
---

A theory gets passed a parameter and executed for each dataset.
It passes only if *all* executions return `true`, else it fails.

Theory example:
```c
#include "test.h"

// Function under test
bool is_odd(int x)
{
  return (x % 2) != 0;
}

THEORY(is_odd_returnes_true_on_odd, x)
{
  return is_odd(x);
}

THEORY(is_odd_returns_false_on_even, x)
{
  return !is_odd(x);
}

FIXTURE_BEGIN

RUN_THEORY(is_odd_returnes_true_on_odd, int, 1, 7, 57425681, 65437249, -5243);
RUN_THEORY(is_odd_returns_false_on_even, int, 2, 4, -653248, 430412);

FIXTURE_END
```

