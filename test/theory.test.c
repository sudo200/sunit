#include "test.h"

THEORY(theory_works, int *params)
{
  return params[0] == params[1];
}

FIXTURE_BEGIN
  RUN_THEORY(theory_works, int *,
    (int[]) { 1, 1 },
    (int[]) { 52345, 52345 },
    (int[]) { 0, 0 },
    (int[]) { -54325, -54325 },
    (int[]) { 999999999, 999999999 }
  );
FIXTURE_END
