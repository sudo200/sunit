#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

#ifndef RESET
#define RESET           "\e[0m"
#endif//RESET

#ifndef PASS_COLOR
#define PASS_COLOR      "\e[32m"
#endif//PASS_COLOR

#ifndef FAIL_COLOR
#define FAIL_COLOR      "\e[31m"
#endif//FAIL_COLOR

#ifndef TEST_TEXT_STYLE
#define TEST_TEXT_STYLE "\e[3m"
#endif//TEST_TEXT_STYLE

#define FACT(name)  static bool name(void)
#define RUN_FACT(name)  do { \
                        fputs("Running test " TEST_TEXT_STYLE #name RESET "...", stderr); \
                        if(!name()) { \
                          fputs(FAIL_COLOR "FAIL" RESET "\n", stderr); \
                          exit(EXIT_FAILURE); \
                        } \
                        \
                        fputs(PASS_COLOR "PASS" RESET "\n", stderr); \
                      } while(false)

#define THEORY(name, param) static bool name(param)
#define RUN_THEORY(name, type, ...) do { \
                        type __test_data[] = { __VA_ARGS__ }; \
                        fputs("Running test " TEST_TEXT_STYLE #name RESET "...", stderr); \
                        for(size_t i = 0; i < sizeof(__test_data)/sizeof(__test_data[0]); i++) \
                          if(!name(__test_data[i])) { \
                            fputs(FAIL_COLOR "FAIL" RESET "\n", stderr); \
                            exit(EXIT_FAILURE); \
                          } \
                        \
                        fputs(PASS_COLOR "PASS" RESET "\n", stderr); \
                      } while(false)

#define FIXTURE_BEGIN int main(void) {
#define FIXTURE_END return EXIT_SUCCESS; }

